package br.com.app.damous.meulivrinho;

import br.com.app.damous.meulivrinho.models.LivrosCatalog;
import retrofit2.Call;
import retrofit2.http.GET;

public interface LivroService {

    public static final String BASE_URL = "http://desafioprodepa2018.getsandbox.com/";

    @GET("catalogo")


    Call<LivrosCatalog> listCatalogo();

}
