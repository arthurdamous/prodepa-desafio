package br.com.app.damous.meulivrinho;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Livro extends RealmObject {
    @PrimaryKey
    private long ID;
    private String titulo;
    private String sinopse;
    private String url_capa;
    private Float preco;


    public Livro() {
    }

    public Livro(String titulo, String sinopse, String url_capa, Float preco) {
        this.titulo = titulo;
        this.sinopse = sinopse;
        this.url_capa = url_capa;
        this.preco = preco;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public String getUrl_capa() {
        return url_capa;
    }

    public void setUrl_capa(String url_capa) {
        this.url_capa = url_capa;
    }

    public Float getPreco() {
        return preco;
    }

    public void setPreco(Float preco) {
        this.preco = preco;
    }
}
