package br.com.app.damous.meulivrinho;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<Livro>  mData;
    RequestOptions option;



    public RecyclerViewAdapter(Context mContext, List<Livro> mData) {
        this.mContext = mContext;
        this.mData = mData;


        option = new RequestOptions().centerCrop().placeholder(R.drawable.ic_launcher_background).error(R.drawable.ic_launcher_background);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardview_item_livro, viewGroup, false);




        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int i) {

        myViewHolder.textView_livro_preco.setText("R$ "+ mData.get(i).getPreco().toString());


        myViewHolder.cardView_livro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent intent = new Intent(mContext,Livro_activity.class);

                intent.putExtra("titulo",mData.get(i).getTitulo());
                intent.putExtra("sinopse",mData.get(i).getSinopse());
                intent.putExtra("preco",mData.get(i).getPreco().toString());
                intent.putExtra("url",mData.get(i).getUrl_capa());

                mContext.startActivity(intent);

            }
        });





        Glide.with(mContext).load(mData.get(i).getUrl_capa()).apply(option).into(myViewHolder.imgView_img_livro);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView textView_livro_titulo;
        TextView textView_livro_preco;
        TextView textView_livro_sinopse;
        ImageView imgView_img_livro;
        CardView cardView_livro;

        public MyViewHolder(View itemView){
            super(itemView);



            textView_livro_preco = (TextView) itemView.findViewById(R.id.precoLivroID);
            imgView_img_livro = (ImageView) itemView.findViewById(R.id.img_livroID);
            cardView_livro = (CardView) itemView.findViewById(R.id.cardviewID);



        }
    }





}
