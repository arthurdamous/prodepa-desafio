package br.com.app.damous.meulivrinho;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.List;
import br.com.app.damous.meulivrinho.models.Catalogo;
import br.com.app.damous.meulivrinho.models.LivrosCatalog;
import io.realm.Realm;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    List<Livro> listadeLivros;
    Livro livro = new Livro();
    private static final String TAG ="damous" ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Intent intent =  new Intent(this,AddLivro_Activity.class);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(intent);

            }
        });

        Realm.init(this);
        Realm realm =Realm.getDefaultInstance();

        realm.addChangeListener(new RealmChangeListener<Realm>() {
            @Override
            public void onChange(Realm realm) {
                iniciaLista();
            }
        });

        listadeLivros = new ArrayList<>();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(LivroService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

         LivroService service = retrofit.create(LivroService.class);
         Call<LivrosCatalog> requestCatalogo = service.listCatalogo();

         requestCatalogo.enqueue(new Callback<LivrosCatalog>() {

             @Override
                 public void onResponse(Call<LivrosCatalog> call, Response<LivrosCatalog> response) {


                     if(!response.isSuccessful()){
                         Log.i(TAG, "Erro:"+ response.code());
                     } else{
                        LivrosCatalog catalog = response.body();



                        int id_contador=210;
                        for(Catalogo c: catalog.catalogo){

                         livro.setTitulo(c.titulo);
                         livro.setSinopse(c.sinopse);
                         livro.setUrl_capa(c.url_capa);
                         livro.setPreco(c.preco);

                         id_contador++;
                         adicionaDB(id_contador,c.titulo,c.sinopse, c.preco,c.url_capa);

                                }

                        }
                        iniciaLista();
                 }


             @Override
             public void onFailure(Call<LivrosCatalog> call, Throwable t) {
                 Log.e(TAG,"Erro:" + t.getMessage());
             }

         });

    }



    public void iniciaLista(){
        Realm.init(this);
        Realm realm =Realm.getDefaultInstance();
        RealmResults<Livro> resultado = realm.where(Livro.class).findAll();




        ArrayList<Livro> listaLivros = new ArrayList<>();

        for(Livro l: resultado){
            listaLivros.add(l);
        }

        listadeLivros = listaLivros;

        RecyclerView myrv = (RecyclerView) findViewById(R.id.recyclerview_ID);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this,listadeLivros);
        myrv.setLayoutManager(new GridLayoutManager(this,3));
        myrv.setAdapter(myAdapter);

    }

    public void adicionaDB(final int id, final String titulo, final String sinopse, final float preco, final String url ){
        Realm.init(this);
        Realm realm = Realm.getDefaultInstance();

      if (realm.where(Livro.class).findAll().isEmpty()){
          realm.executeTransactionAsync(new Realm.Transaction() {
              @Override
              public void execute(Realm bgRealm) {
                  Livro livro = bgRealm.createObject(Livro.class, id);
                  livro.setTitulo(titulo);
                  livro.setSinopse(sinopse);
                  livro.setPreco(preco);
                  livro.setUrl_capa(url);


              }
          }, new Realm.Transaction.OnSuccess() {
              @Override
              public void onSuccess() {
                  Log.i(TAG,"Adicionado com Sucesso");
              }
          }, new Realm.Transaction.OnError() {
              @Override
              public void onError(Throwable error) {
                  Log.e(TAG,"Deu erro:" + error.getMessage());
              }
          });


      }else{
          Log.i(TAG,"Lista ja preenchida");


      }

    }

    public void deletaTudo(){
        Realm.init(this);
        Realm realm = Realm.getDefaultInstance();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Livro.class).findAll().deleteAllFromRealm();
                Log.i(TAG,"Excluido com sucesso");
            }
        });


    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
