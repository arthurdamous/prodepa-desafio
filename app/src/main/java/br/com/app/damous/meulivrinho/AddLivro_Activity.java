package br.com.app.damous.meulivrinho;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import java.util.Random;

import io.realm.Realm;

public class AddLivro_Activity extends AppCompatActivity {

    EditText preco;
    EditText titulo;
    EditText sinopse;
    EditText url;

    final static String TAG = "damous";
    String titulo_livro, sinopse_livro, preco_livro, url_livro;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_livro_);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        titulo = findViewById(R.id.tituloID_add);
        sinopse = findViewById(R.id.sinopseID_add);
        url = findViewById(R.id.urlID_add);
        preco = findViewById(R.id.precoID_add);

        SimpleMaskFormatter smf = new SimpleMaskFormatter("R$ NN.NN");
        MaskTextWatcher mtw = new MaskTextWatcher(preco, smf);
        preco.addTextChangedListener(mtw);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escolha();

            }
        });


    }

    public void escolha(){
        titulo_livro = titulo.getText().toString();
        sinopse_livro = sinopse.getText().toString();
        url_livro = url.getText().toString();
        preco_livro = preco.getText().toString();

        String precosemForm;
        String precosemForm3;

        precosemForm = preco_livro.replace('$', ' ');
        precosemForm3 = precosemForm.replace('R', ' ');



        if (!titulo_livro.isEmpty() && !sinopse_livro.isEmpty() && !url_livro.isEmpty() && !preco_livro.isEmpty()) {
            adicionaLivro(precosemForm3);


        }else{
            Toast.makeText(this, "Preencha todos os campos", Toast.LENGTH_SHORT).show();

        }
    }


    public void adicionaLivro(final String precosemForm1) {


        Intent intent = new Intent(this, MainActivity.class);


        Random randomico = new Random();
        final int id_livro = randomico.nextInt((999) + 1);

            Realm.init(this);
            Realm realm = Realm.getDefaultInstance();
            final Float precodoLivro = Float.parseFloat(precosemForm1);


            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm bgRealm) {
                    Livro livro = bgRealm.createObject(Livro.class, id_livro);
                    livro.setTitulo(titulo_livro);
                    livro.setSinopse(sinopse_livro);
                    livro.setPreco(precodoLivro);
                    livro.setUrl_capa(url_livro);


                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    Log.i(TAG, "Adicionado com Sucesso");
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    Log.e(TAG, "Deu erro:" + error.getMessage());
                }

            });
            startActivity(intent);
        }

    }





