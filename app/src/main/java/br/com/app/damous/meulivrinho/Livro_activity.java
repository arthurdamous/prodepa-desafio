package br.com.app.damous.meulivrinho;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class Livro_activity extends AppCompatActivity {

    private ImageView img_livro;
    private TextView nome_livro,sinopse_livro,preco_livro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_livro_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Livro adicionado aos favoritos", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        nome_livro = findViewById(R.id.tv_titulo_id);
        sinopse_livro   =  findViewById(R.id.tv_sinopse_id);
        preco_livro = findViewById(R.id.tv_preco_livro_id);
        img_livro = findViewById(R.id.img_livro_id);




        Intent intent =  getIntent();

        String titulo = getIntent().getStringExtra("titulo");
        String sinopse = getIntent().getStringExtra("sinopse");
        String preco = getIntent().getStringExtra("preco");
        String url_capa = getIntent().getStringExtra("url");



        nome_livro.setText(titulo);
        sinopse_livro.setText(sinopse);
        preco_livro.setText("R$ "+preco);

        Glide.with(this).load(url_capa).into(img_livro);



    }

}
